import React from "react";
import ReactDOM from "react-dom";

export default function Dashboard() {
    return <div>Dashboard</div>;
}

if (document.getElementById("dashboard")) {
    ReactDOM.render(<Dashboard />, document.getElementById("dashboard"));
}
